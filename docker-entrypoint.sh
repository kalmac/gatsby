#!/bin/bash

# This entrypoint is a workaround for CTR+C bug
# see https://stackoverflow.com/questions/41097652/how-to-fix-ctrlc-inside-a-docker-container
$@
