FROM node:latest

RUN apt-get -y update; \
    apt-get -y install git; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*

RUN npm install -g gatsby-cli

COPY docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
